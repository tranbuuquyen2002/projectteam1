package fpt.intern.fa_api.model.mapper;


import fpt.intern.fa_api.model.filter.ClassEntityFilter;
import fpt.intern.fa_api.model.request.ClassSearchCriteria;
import fpt.intern.fa_api.model.response.ClassEntityPageResponse;
import org.mapstruct.Mapper;

import java.util.List;

import fpt.intern.fa_api.model.entity.ClassEnity;
import fpt.intern.fa_api.model.request.ClassRequest;
import fpt.intern.fa_api.model.response.ClassResponse;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface ClassMapper {

    // Map Entity to Response
    ClassResponse toResponse(ClassEnity classenity);

    List<ClassResponse> toResponseList(List<ClassEnity> classenitylist);
    
    ClassEnity toEntity(ClassRequest request);

    ClassEntityFilter toFilter(ClassSearchCriteria criteria);

    default ClassEntityPageResponse<ClassResponse> toClassEntityPageResponse(Page<ClassEnity> page) {
        ClassEntityPageResponse<ClassResponse> pageResponse = new ClassEntityPageResponse<>();
        pageResponse.setContent(toResponseList(page.getContent()));
        pageResponse.setPage(page.getNumber());
        pageResponse.setSize(page.getSize());
        pageResponse.setTotalPages(page.getTotalPages());
        pageResponse.setTotalSize(page.getTotalElements());
        return pageResponse;
    }
}
