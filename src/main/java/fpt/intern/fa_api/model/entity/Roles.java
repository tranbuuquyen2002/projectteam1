package fpt.intern.fa_api.model.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fpt.intern.fa_api.model.entity.enums.RoleStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name ="Roles")
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name may not be blank")
    @Column(nullable = false)
    private String name;  

    private String description;

    private RoleStatus status;

    private Date createdDate;

    private Date modifiedDate;

    @OneToMany(mappedBy = "Roles", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<User> userList;

    @ManyToMany(mappedBy = "roles")
    private List<Permission> permission;
}
