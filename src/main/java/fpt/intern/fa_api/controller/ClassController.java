package fpt.intern.fa_api.controller;

import fpt.intern.fa_api.exception.ApplicationException;
import fpt.intern.fa_api.exception.NotFoundException;
import fpt.intern.fa_api.exception.ValidationException;
import fpt.intern.fa_api.model.entity.ClassEnity;
import fpt.intern.fa_api.model.enums.ClassStatus;
import fpt.intern.fa_api.model.mapper.ClassMapper;
import fpt.intern.fa_api.model.request.ClassRequest;
import fpt.intern.fa_api.model.filter.ClassEntityFilter;
import fpt.intern.fa_api.model.filter.ClassEntitySpec;
import fpt.intern.fa_api.model.request.ClassSearchCriteria;
import fpt.intern.fa_api.model.response.ApiResponse;
import fpt.intern.fa_api.model.response.ClassEntityPageResponse;
import fpt.intern.fa_api.model.response.ClassResponse;
import fpt.intern.fa_api.repository.ClassRepository;
import fpt.intern.fa_api.repository.UserRepository;
import fpt.intern.fa_api.service.ClassService;
import fpt.intern.fa_api.util.ClassServiceUtil;
import jakarta.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/fa/api")
public class ClassController {

	@Autowired
	ClassService classRepository;

	@PostMapping(value = { "/create/step1" })
	public ResponseEntity<ApiResponse> getname(@Valid @RequestBody ClassRequest classRequest,
			BindingResult bindingResult) {
		try {

			ClassServiceUtil classServiceUtil = new ClassServiceUtil();
			String code = classServiceUtil.saveClass();
			classRequest.setCode(code);
			classRequest.setStatus(ClassStatus.PLANNING);
			// Save
			ClassResponse classResponse = classRepository.save(classRequest, bindingResult);

			// Response
			ApiResponse apiResponse = new ApiResponse();
			apiResponse.ok(classResponse);
			return ResponseEntity.ok(apiResponse);
		} catch (NotFoundException ex) {
			throw ex; // Rethrow NotFoundException
		} catch (ValidationException ex) {
			throw ex; // Rethrow ValidationException
		} catch (Exception ex) {
			throw new ApplicationException(); // Handle other exceptions
		}
	}

	@PostMapping(value = { "/create/step2" })
	public ResponseEntity<ApiResponse> save(@Valid @RequestBody ClassRequest classRequest,
			BindingResult bindingResult) {
		try {
			// Save
			ClassResponse classResponse = classRepository.update(classRequest, bindingResult);

			// Response
			ApiResponse apiResponse = new ApiResponse();
			apiResponse.ok(classResponse);
			return ResponseEntity.ok(apiResponse);
		} catch (NotFoundException ex) {
			throw ex; // Rethrow NotFoundException
		} catch (ValidationException ex) {
			throw ex; // Rethrow ValidationException
		} catch (Exception ex) {
			throw new ApplicationException(); // Handle other exceptions
		}
	}

    @GetMapping("/list")
    public ResponseEntity<ApiResponse<List<ClassResponse>>> getAllClassWithPagination(@RequestParam Map<String, String> customQuery) {
        try {

            boolean checkPage = customQuery.containsKey("page");      //check trường offset
            boolean checkLimit = customQuery.containsKey("limit");    //check trường limit

            int page = checkPage? Integer.parseInt(customQuery.get("page")):0;      //Nếu không có page thì mặc định page ở trang 0
            int limit = checkLimit? Integer.parseInt(customQuery.get("limit")):5;   //Nếu không có limit thì mặc định limit = 5
			List<ClassResponse> ListclassResponse = classRepository.findClassWithPagination(page, limit);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(ListclassResponse);
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } catch (Exception ex) {
            throw new ApplicationException();
        }
    }

    @GetMapping("/list/{field}")
    public ResponseEntity<ApiResponse<List<ClassResponse>>> getClassWithPaginationAndSort(@RequestParam Map<String, String> customQuery, @PathVariable String field) {
        try {
            boolean checkPage = customQuery.containsKey("page");      //check trường offset
            boolean checkLimit = customQuery.containsKey("limit");    //check trường limit

            int page = checkPage? Integer.parseInt(customQuery.get("page")):0;      //Nếu không có page thì mặc định page ở trang 0
            int limit = checkLimit? Integer.parseInt(customQuery.get("limit")):5;   //Nếu không có limit thì mặc định limit = 5

            List<ClassResponse> ListclassResponse = classRepository.findClassWithPaginationAndSorting(page, limit, field);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(ListclassResponse);
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } catch (Exception ex) {
            throw new ApplicationException();
        }
    }

    @PostMapping(value = {"/search-with-code-and-name"})
    public ResponseEntity<ApiResponse> findByCodeAndName(@RequestParam String code, @RequestParam String name) {
        try {
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(classRepository.findByCodeAndName(code, name));
            return ResponseEntity.ok(apiResponse);
        } catch (NotFoundException ex) {
            throw ex;
        }
    }

    @PostMapping("/filter")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ApiResponse> getCars(@RequestBody ClassSearchCriteria criteria) {
        ClassEntityFilter classEntityFilter = new ClassEntityFilter(criteria.getLocations(),criteria.getDateFrom(), criteria.getDateTo(), criteria.getTimeFrom(), criteria.getTimeTo(), criteria.getClassStatus(), criteria.getFsu());
        ApiResponse response = new ApiResponse();
        response.setPayload(classRepository.searchClass(classEntityFilter, criteria.getPage(), criteria.getSize()));
        return ResponseEntity.ok().body(response);
    }
}
